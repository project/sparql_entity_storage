<?php

declare(strict_types=1);

namespace Drupal\sparql_entity_storage\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Base class for events dispatched when storing/loading values from storage.
 */
abstract class ValueEventBase extends Event {

  /**
   * The entity type id.
   */
  protected string $entityTypeId;

  /**
   * The field name.
   */
  protected string $field;

  /**
   * The field column value.
   *
   * @var mixed
   */
  protected $value;

  /**
   * The value format.
   */
  protected ?string $format;

  /**
   * The field language. Defaults to NULL.
   */
  protected ?string $lang = NULL;

  /**
   * The field column. Defaults to NULL.
   */
  protected ?string $column = NULL;

  /**
   * The entity bundle. Defaults to NULL.
   */
  protected ?string $bundle = NULL;

  /**
   * An associative array with information on the mapping of the field.
   *
   * @var array
   */
  protected array $fieldMappingInfo;

  /**
   * Instantiates a new ValueEventBase event object.
   *
   * @param string $entity_type_id
   *   The entity type id.
   * @param string $field
   *   The field name.
   * @param mixed $value
   *   The field column value.
   * @param array $field_mapping_info
   *   An associative array with information on the mapping of the field.
   * @param null|string $lang
   *   The field language.
   * @param null|string $column
   *   The field column.
   * @param null|string $bundle
   *   The entity bundle.
   * @param string|null $format
   *   The value format.
   */
  public function __construct(string $entity_type_id, string $field, $value, array $field_mapping_info, ?string $lang = NULL, ?string $column = NULL, ?string $bundle = NULL, ?string $format = NULL) {
    $this->entityTypeId = $entity_type_id;
    $this->field = $field;
    $this->value = $value;
    $this->lang = $lang;
    $this->column = $column;
    $this->bundle = $bundle;
    $this->fieldMappingInfo = $field_mapping_info;
    $this->format = $format;
  }

  /**
   * Returns the entity type id.
   *
   * @return string
   *   The entity type id.
   */
  public function getEntityTypeId(): string {
    return $this->entityTypeId;
  }

  /**
   * Returns the field name.
   *
   * @return string
   *   The field name.
   */
  public function getField(): string {
    return $this->field;
  }

  /**
   * Returns the field column value.
   *
   * @return mixed
   *   The field column value.
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Sets the field column value.
   *
   * @param string $value
   *   The value to set.
   *
   * @return $this
   */
  public function setValue($value): self {
    $this->value = $value;
    return $this;
  }

  /**
   * Returns the value format.
   *
   * @return string
   *   The value format.
   */
  public function getFormat(): string {
    return $this->format;
  }

  /**
   * Sets the value format.
   *
   * @param string $format
   *   Sets the value format.
   *
   * @return $this
   */
  public function setFormat(string $format): self {
    $this->format = $format;
    return $this;
  }

  /**
   * Returns an associative array with information on the mapping of the field.
   *
   * @return array
   *   The array filled with information about the field mappings.
   */
  public function getFieldMappingInfo(): array {
    return $this->fieldMappingInfo;
  }

  /**
   * Returns the field language.
   *
   * @return null|string
   *   The field language, or NULL if not available.
   */
  public function getLang(): ?string {
    return $this->lang;
  }

  /**
   * Returns the field column.
   *
   * @return null|string
   *   The field column, or NULL if not available.
   */
  public function getColumn(): ?string {
    return $this->column;
  }

  /**
   * Returns the entity bundle.
   *
   * @return null|string
   *   The entity bundle, or NULL if not available.
   */
  public function getBundle(): ?string {
    return $this->bundle;
  }

}
