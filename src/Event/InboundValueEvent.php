<?php

declare(strict_types=1);

namespace Drupal\sparql_entity_storage\Event;

/**
 * An event dispatched when a value has been loaded from storage.
 */
class InboundValueEvent extends ValueEventBase {}
