<?php

declare(strict_types=1);

namespace Drupal\sparql_entity_storage\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a entity ID generator plugin annotation.
 *
 * @see plugin_api
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class SparqlEntityIdGenerator extends Plugin {

  /**
   * The entity ID generator plugin id.
   */
  public string $id;

  /**
   * The human-readable name of the entity ID generator plugin.
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

}
