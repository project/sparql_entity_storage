<?php

namespace Drupal\sparql_entity_storage\Driver\Database\sparql\Install;

use Drupal\Core\Database\Install\Tasks as InstallTasks;

/**
 * Specifies installation tasks for SPARQL driver.
 */
class Tasks extends InstallTasks {

  /**
   * {@inheritdoc}
   */
  public function name(): string {
    return $this->t('SPARQL');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormOptions(array $database): array {
    return [];
  }

}
