<?php

declare(strict_types=1);

namespace Drupal\sparql_entity_storage\Exception;

/**
 * Thrown when a Sparql query fails to execute.
 */
class SparqlQueryException extends \Exception {}
