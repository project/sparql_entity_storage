<?php

declare(strict_types=1);

namespace Drupal\sparql_entity_storage\Exception;

/**
 * Thrown an unsupported character is used in a SPARQL query.
 *
 * Note that the unsupported character, does not necessarily mean that the
 * character is not supported by SPARQL, but that it is not supported by the
 * SparqlEntityStorage class.
 */
class UnsupportedCharactersException extends \Exception {}
