<?php

declare(strict_types=1);

namespace Drupal\Tests\sparql_entity_storage\Unit;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\sparql_entity_storage\Event\ActiveGraphEvent;
use Drupal\sparql_entity_storage\ParamConverter\SparqlEntityStorageConverter;
use Drupal\sparql_entity_storage\SparqlEntityStorageInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Route;

/**
 * Test description.
 *
 * @group sparql_entity_storage
 *
 * @coversDefaultClass \Drupal\sparql_entity_storage\ParamConverter\SparqlEntityStorageConverter
 */
final class ParameterConverterTest extends UnitTestCase {

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * Entity repository mock.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityRepository;

  /**
   * Event dispatcher mock.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $eventDispatcher;

  /**
   * The converter instance.
   *
   * @var \Drupal\sparql_entity_storage\ParamConverter\SparqlEntityStorageConverter
   */
  protected SparqlEntityStorageConverter $paramConverter;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $content = $this->createMock(ContentEntityInterface::class);
    $sqlStorage = $this->createMock(ContentEntityStorageInterface::class);
    $sqlStorage->method('load')->willReturn($content);
    $sparqlStorage = $this->createMock(SparqlEntityStorageInterface::class);
    $sparqlStorage->method('load')->willReturn($content);
    $configStorage = $this->createMock(ConfigEntityStorageInterface::class);
    $typesPerStorage = [
      'entity_config' => $configStorage,
      'entity_sparql' => $sparqlStorage,
      'entity_sql' => $sqlStorage,
    ];

    $this->entityRepository = $this->createMock(EntityRepositoryInterface::class);
    $this->eventDispatcher = $this->createMock(EventDispatcherInterface::class);

    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityTypeManager->expects($this->any())
      ->method('hasDefinition')
      ->willReturnCallback(
        function (string $entity_type) use ($typesPerStorage): bool {
          return in_array($entity_type, array_keys($typesPerStorage));
        }
      );
    $this->entityTypeManager->expects($this->any())
      ->method('getStorage')
      ->willReturnCallback(
        function (string $entity_type) use ($typesPerStorage) {
          $storage = $typesPerStorage[$entity_type] ?? NULL;
          if ($storage) {
            return $storage;
          }
          throw new PluginNotFoundException($entity_type);
        }
      );

    $this->entityRepository->method('getTranslationFromContext')
      ->willReturnArgument(0);
    $this->entityRepository->method('getCanonical')
      ->willReturnCallback(
        function (string $entity_type, int|string $id) use ($typesPerStorage) {
          $storage = $typesPerStorage[$entity_type] ?? NULL;
          if ($storage) {
            return $storage->load($id);
          }
          throw new PluginNotFoundException($entity_type);
        }
      );

    $this->paramConverter = new SparqlEntityStorageConverter(
      $this->entityTypeManager,
      $this->entityRepository,
      $this->eventDispatcher,
    );
  }

  /**
   * Tests parameter conversion with invalid URIs or non-matching storage types.
   *
   * @covers ::convert
   *
   * @dataProvider providerTestConvert
   */
  public function testConvert(string $uri, string $converter_type, bool $should_convert, bool $should_dispatch): void {
    $dispatchExpectation = $should_dispatch ? $this->atLeastOnce() : $this->never();
    $this->eventDispatcher->expects($dispatchExpectation)->method('dispatch')
      ->willReturnCallback(function (ActiveGraphEvent $event) {
        return $event->setGraphs(['http://foo.example.com/thing']);
      });

    $value = $this->paramConverter->convert(
      $uri,
      ['type' => $converter_type],
      'dummy name',
      []
    );
    if (empty($should_convert)) {
      $this->assertNull($value);
      return;
    }

    $this->assertInstanceOf(EntityInterface::class, $value);
  }

  /**
   * Data provider for testConvert().
   *
   * @return array[]
   *   The test cases.
   */
  public static function providerTestConvert(): array {
    return [
      'Space in URI' => [
        'uri' => 'http://example.com/some space',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Curly brackets in URI' => [
        'uri' => 'http://example.com/{curly-brackets}',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Angle brackets in URI' => [
        'uri' => 'http://example.com/<angle-brackets>',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Backslash in URI' => [
        'uri' => 'http://example.com/\\backslash',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Pipe in URI' => [
        'uri' => 'http://example.com/|pipe',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Backtick in URI' => [
        'uri' => 'http://example.com/`backtick`',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Caret in URI' => [
        'uri' => 'http://example.com/^caret',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Double quote in URI' => [
        'uri' => 'http://example.com/"double-quote',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Missing entity type ID' => [
        'uri' => 'notanuri',
        'converter_type' => 'entity:missing',
        'should_convert' => FALSE,
        'should_dispatch' => FALSE,
      ],
      'Valid ID of a non SparQL-stored entity' => [
        'uri' => 'notanuri',
        'converter_type' => 'entity:entity_sql',
        'should_convert' => TRUE,
        'should_dispatch' => FALSE,
      ],
      'Valid URI of a SparQL-stored entity' => [
        'uri' => 'http://foo.example.com/thing',
        'converter_type' => 'entity:entity_sparql',
        'should_convert' => TRUE,
        'should_dispatch' => TRUE,
      ],
    ];
  }

  /**
   * Tests the applies() method.
   *
   * @covers ::applies
   *
   * @dataProvider providerTestApplies
   */
  public function testApplies(array $definition, $name, Route $route, $applies): void {
    $this->assertEquals(
      $applies,
      $this->paramConverter->applies($definition, $name, $route)
    );
  }

  /**
   * Provides test data for testApplies()
   */
  public static function providerTestApplies(): array {
    return [
      'Missing type definition' => [
        ['type' => 'entity:foo'],
        'foo',
        new Route('/test/bar'),
        FALSE,
      ],
      'SparQL-stored entity type' => [
        ['type' => 'entity:entity_sparql'],
        'foo',
        new Route('/test/bar'),
        TRUE,
      ],
      'SQL-stored entity type' => [
        ['type' => 'entity:entity_sql'],
        'foo',
        new Route('/test/bar'),
        FALSE,
      ],
      'Config entity type' => [
        ['type' => 'entity:entity_config'],
        'foo',
        new Route('/test/bar'),
        FALSE,
      ],
      'Dynamic entity type' => [
        ['type' => 'entity:{entity_type}'],
        'foo',
        new Route('/test/{entity_type}/{id}'),
        TRUE,
      ],
    ];
  }

}
