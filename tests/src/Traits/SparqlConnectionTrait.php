<?php

declare(strict_types=1);

namespace Drupal\Tests\sparql_entity_storage\Traits;

use Drupal\Core\Database\Database;
use Drupal\sparql_entity_storage\Driver\Database\sparql\ConnectionInterface;
use DrupalFinder\DrupalFinderComposerRuntime;

/**
 * Provides helpers to add a SPARQL database connection in tests.
 */
trait SparqlConnectionTrait {

  /**
   * The SPARQL database connection.
   */
  protected ?ConnectionInterface $sparql;

  /**
   * The SPARQL database info.
   *
   * @var array|null
   */
  protected ?array $sparqlConnectionInfo;

  /**
   * Checks if the triple store is an Virtuoso 6 instance.
   *
   * @throws \Exception
   *   When Virtuoso version is 6.
   */

  /**
   * Configures the DB connection to the triple store.
   *
   * @throws \LogicException
   *   When SIMPLETEST_SPARQL_DB is not set.
   */
  protected function setUpSparql() {
    $db_url = getenv('SIMPLETEST_SPARQL_DB');
    if (empty($db_url)) {
      // No connection to the Sparql database was defined. Set the default url
      // used for testing on DrupalCI.
      $db_url = 'sparql://virtuoso:8890?module=sparql_entity_storage';
    }

    if (!defined('DRUPAL_ROOT')) {
      $drupalFinder = new DrupalFinderComposerRuntime();
      $root = $drupalFinder->getDrupalRoot();
      require_once "$root/core/includes/bootstrap.inc";
    }

    $this->sparqlConnectionInfo = Database::convertDbUrlToConnectionInfo($db_url, DRUPAL_ROOT);
    $this->sparqlConnectionInfo['namespace'] = 'Drupal\\sparql_entity_storage\\Driver\\Database\\sparql';
    Database::addConnectionInfo('sparql_default', 'default', $this->sparqlConnectionInfo);

    $this->sparql = Database::getConnection('default', 'sparql_default');
  }

}
