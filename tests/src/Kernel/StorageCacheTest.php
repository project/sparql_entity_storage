<?php

declare(strict_types=1);

namespace Drupal\Tests\sparql_entity_storage\Kernel;

use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\State\StateInterface;
use Drupal\sparql_test\Entity\TestSparql;

/**
 * Tests the caching mechanism of the storage class.
 *
 * @group sparql_entity_storage
 */
class StorageCacheTest extends SparqlKernelTestBase {

  /**
   * The static memory instance.
   */
  protected MemoryCacheInterface $memoryCache;

  /**
   * The state service.
   */
  protected StateInterface $state;

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);
    if ($container->hasDefinition('entity.memory_cache')) {
      $container->getDefinition('entity.memory_cache')
        ->setClass(TestMemoryBackend::class)
        ->addArgument($this->container->get('state'));
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->memoryCache = $this->container->get('entity.memory_cache');
    $this->state = $this->container->get('state');
  }

  /**
   * Tests the memory and the persistent cache.
   */
  public function testMemoryCache(): void {
    $entity = TestSparql::create([
      'type' => 'fruit',
      'id' => 'http://example.com/apple',
      'title' => 'Apple',
    ]);
    $entity->save();

    // No cache is created when the entity is inserted.
    $this->assertTestCallsEmpty();

    $this->assertTestCallsEmpty();
    TestSparql::load($entity->id());

    // An attempt to fetch the entity was made but no results were returned.
    $this->assertTestCallsContainCall('get', 'values:sparql_test:http://example.com/apple:default');
    // The entity was set in the memory cache.
    $this->assertTestCallsContainCall('set', 'values:sparql_test:http://example.com/apple:default');
    $this->assertCount(2, $this->state->get(TestMemoryBackend::STATE_VARIABLE_NAME, []));

    // Reset the calls.
    $this->state->set(TestMemoryBackend::STATE_VARIABLE_NAME, []);
    TestSparql::load($entity->id());

    // An attempt to fetch the entity was made but no results were returned.
    $this->assertTestCallsContainCall('get', 'values:sparql_test:http://example.com/apple:default', TRUE);
    $this->assertCount(1, $this->state->get(TestMemoryBackend::STATE_VARIABLE_NAME, []));

    $this->state->set(TestMemoryBackend::STATE_VARIABLE_NAME, []);
    // Reset the memory cache and reload.
    $this->container->get('entity_type.manager')->getStorage('sparql_test')->resetCache([$entity->id()]);
    TestSparql::load($entity->id());
    $this->assertTestCallsContainCall('get', 'values:sparql_test:http://example.com/apple:default');
    $this->assertTestCallsContainCall('set', 'values:sparql_test:http://example.com/apple:default');
    $this->assertCount(2, $this->state->get(TestMemoryBackend::STATE_VARIABLE_NAME, []));
  }

  /**
   * Tests that the method calls from the test memory backend contain an entry.
   */
  protected function assertTestCallsEmpty(): void {
    $this->state->resetCache();
    $this->assertEmpty($this->state->get(TestMemoryBackend::STATE_VARIABLE_NAME, []));
  }

  /**
   * Tests that the method calls from the test memory backend contain an entry.
   *
   * @param string $method
   *   Either get or set.
   * @param string $cid
   *   The cache ID related to the check.
   * @param bool $has_return
   *   (optional) Whether the method call should have a return value. Defaults
   *   to FALSE.
   */
  protected function assertTestCallsContainCall(string $method, string $cid, bool $has_return = FALSE): void {
    $calls = $this->state->get(TestMemoryBackend::STATE_VARIABLE_NAME, []);
    foreach ($calls as $call) {
      if ($call['method'] === $method && $call['cid'] === $cid && !empty($call['has_return']) === $has_return) {
        return;
      }
    }

    $this->fail(sprintf('The method call %s with the cid %s, %s results, was not found.', $method, $cid, $has_return ? 'with' : 'without'));
  }

}
