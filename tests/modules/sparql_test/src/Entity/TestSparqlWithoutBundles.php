<?php

declare(strict_types=1);

namespace Drupal\sparql_test\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines a testing entity without bundles.
 *
 * @ContentEntityType(
 *   id = "sparql_without_bundles_test",
 *   label = @Translation("SPARQL without bundled test entity"),
 *   handlers = {
 *     "storage" = "\Drupal\sparql_entity_storage\SparqlEntityStorage",
 *   },
 *   base_table = null,
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *   },
 *   links = {
 *     "canonical" = "/sparql_without_bundles_test/{sparql_test}",
 *     "edit-form" = "/sparql_without_bundles_test/{sparql_test}/edit",
 *     "delete-form" = "/sparql_without_bundles_test/{sparql_test}/delete",
 *     "collection" = "/sparql_without_bundles_test/list"
 *   },
 * )
 */
class TestSparqlWithoutBundles extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('uri')->setLabel(t('ID'));

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setTranslatable(FALSE)
      ->setRequired(TRUE);

    return $fields;
  }

}
